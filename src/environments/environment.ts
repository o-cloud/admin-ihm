// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  urlJsonServer: `http://${location.host}/api/generic-backend`,
  webSocketSearchCatalog: `ws://${location.host}/api/generic-backend/catalog/data/search`,
  urlCatalog: `http://${location.host}/api/catalog/public`,
  urlServiceProvider: `http://${location.host}/api/service-provider`,
  urlDiscovery: `http://${location.host}/api/discovery`,
  urlPipelineOrchestrator: `http://${location.host}/api/pipeline-orchestrator`,
  urlProviders: `http://${location.host}/api/providers`,
  ipfsEntrypoints: [
    {"name": "ipfs", "port": 4001},
    {"name": "ipfs2", "port": 4002},
    {"name": "ipfs3", "port": 4003},
    {"name": "ipfs4", "port": 4004},
    {"name": "ipfs5", "port": 4005},
  ],
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
