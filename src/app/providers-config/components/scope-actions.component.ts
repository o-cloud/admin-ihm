import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
  template: `
    <div style="display: flex; justify-content: space-between">
      <app-button (click)="onEdit()" nbTooltip="Update scope"
        ><nb-icon icon="edit"></nb-icon
      ></app-button>
      <app-button (click)="onDelete()" nbTooltip="Delete scope"
        ><nb-icon icon="trash"></nb-icon
      ></app-button>
    </div>
  `,
  styleUrls: ['../providers-config.component.scss'],
})
export class ScopeActionsComponent implements ViewCell {
  constructor() {}
  @Input()
  value!: string | number;
  @Input() rowData: any;

  @Output() delete: EventEmitter<any> = new EventEmitter();
  @Output() edit: EventEmitter<any> = new EventEmitter();

  onEdit(): void {
      this.edit.emit(this.rowData);
  }

  onDelete(): void {
      this.delete.emit(this.rowData);
  }
}
