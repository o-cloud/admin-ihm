import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProvidersConfigComponent } from './providers-config.component';
import { SharedModule } from '../@shared/shared.module';
import { ProvidersConfigRoutingModule } from './providers-config-routing.module';
import {
  NbAccordionModule,
  NbCardModule,
  NbDialogModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbSelectModule,
  NbSpinnerModule,
} from '@nebular/theme';
import { CreateScopeComponent } from './create-scope/create-scope.component';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '../@core/core.module';
import { UpdateProviderComponent } from './update-provider/update-provider.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ScopeActionsComponent } from './components/scope-actions.component';

@NgModule({
  declarations: [
    ProvidersConfigComponent,
    CreateScopeComponent,
    UpdateProviderComponent,
    ScopeActionsComponent
  ],
  imports: [
    CommonModule,
    ProvidersConfigRoutingModule,
    NbCardModule,
    NbIconModule,
    NbAccordionModule,
    FormsModule,
    NbInputModule,
    NbFormFieldModule,
    NbSelectModule,
    NbSpinnerModule,
    NbDialogModule.forChild(),
    CoreModule,
    Ng2SmartTableModule,
    SharedModule,
  ],
})
export class ProvidersConfigModule {}
