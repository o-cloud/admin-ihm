import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProvidersConfigComponent } from './providers-config.component';

const routes: Routes = [
  {
    path: 'providers-config',
    component: ProvidersConfigComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProvidersConfigRoutingModule {}
