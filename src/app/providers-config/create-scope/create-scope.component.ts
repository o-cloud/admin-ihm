import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { ProviderService } from 'src/app/@core/services/provider.service';
import {
  Permissions,
  Provider,
  Scope,
} from '../../@core/models/provider.model';

@Component({
  selector: 'app-scope-service',
  templateUrl: './create-scope.component.html',
  styleUrls: ['./create-scope.component.scss'],
})
export class CreateScopeComponent implements OnInit {
  oldScope: Scope | null = null;
  provider: Provider = {
    id: '',
    type: '',
    version: '',
    name: '',
    images: [''],
    description: '',
    metadata: '',
    createdAt: '',
    updatedAt: '',
    registeredAt: '',
    unregisteredAt: '',
    endpointLocation: '',
    workflowStructure: '',
    catalogSyncStatus: 0,
    scopes: [],
  };

  resources: string[] = [];

  edit: any;

  scope?: Scope;

  scopeForm: Scope = {
    id: '',
    name: '',
    description: '',
    resources: [],
    permissions: [],
  };

  permissions?: Permissions;

  error: any = {
    All: '',
    Name: '',
    Version: '',
    Description: '',
    Cwl: '',
    Images: '',
  };

  constructor(
    private providerService: ProviderService,
    protected dialogRef: NbDialogRef<CreateScopeComponent>
  ) {}

  ngOnInit(): void {
    if (this.edit) {
      this.scopeForm = Object.assign({}, this.scope);
    }
  }

  create(): void {
    this.providerService.addScope(this.provider, this.scopeForm).subscribe(
      () => {
        this.dialogRef.close(this.scopeForm);
      },
      (err) => {
        err = err.error.errors;
        if (err.hasOwnProperty('422001')) {
          this.error.All = '';
          this.error = err['422001'];
        } else if (err.hasOwnProperty('422002')) {
          this.error.All = err['422002'];
        }
      }
    );
  }

  update(): void {
    this.providerService.updateScope(this.provider, this.scopeForm).subscribe(
      () => {
        this.dialogRef.close(this.scopeForm);
      },
      (err) => {
        err = err.error.errors;
        if (err.hasOwnProperty('422001')) {
          this.error.All = '';
          this.error = err['422001'];
        } else if (err.hasOwnProperty('422002')) {
          this.error.All = err['422002'];
        }
      }
    );
  }

  close(): void {
    this.dialogRef.close();
  }
}
