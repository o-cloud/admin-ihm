import { Component, OnInit } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { Subscription } from 'rxjs';
import { Permissions, Provider, Scope } from '../@core/models/provider.model';
import { ProviderService } from '../@core/services/provider.service';
import { ModalsService } from '../@shared/validation-modal/modals.service';
import { CreateScopeComponent } from './create-scope/create-scope.component';
import { UpdateProviderComponent } from './update-provider/update-provider.component';

@Component({
  selector: 'app-providers-config',
  templateUrl: './providers-config.component.html',
  styleUrls: ['./providers-config.component.scss'],
})
export class ProvidersConfigComponent implements OnInit {
  providers: Provider[] = [];
  scopes: Scope[] = [];
  resources: string[] = [];
  permissions?: Permissions;
  selectedProvider: Provider | undefined;
  subscriptions = new Subscription();
  loading = false;
  loadingScope = false;

  constructor(
    private providerService: ProviderService,
    private modalService: ModalsService,
    protected dialogService: NbDialogService
  ) {}

  ngOnInit(): void {
    this.getAllProviders();
  }

  getAllProviders() {
    this.loading = true;

    this.providerService.getAllProviders().subscribe((data) => {
      this.loading = false;
      this.providers = data;
      if (this.selectedProvider) {
        let selectedProv = this.providers.find(
          (p) =>
            p.name === this.selectedProvider!.name &&
            p.version === this.selectedProvider!.version
        );

        if (selectedProv) {
          this.selectedProvider = selectedProv;
        }
      }

      if (!this.selectedProvider) {
        this.selectedProvider = this.providers[0];
      }
      this.getScopes(this.selectedProvider);
      this.getResources(this.selectedProvider);
      this.getPermissions(this.selectedProvider);
    });
  }

  selectProvider(provider: Provider) {
    this.getScopes(provider);
    this.selectedProvider = provider;
    this.scopes = provider.scopes;
  }

  addProviderToCatalog() {
    if (this.selectedProvider) {
      this.selectedProvider.catalogSyncStatus = 1;
      this.providerService
        .addToCatalog(this.selectedProvider)
        .subscribe(() => this.getAllProviders());
    }
  }

  removeProviderFromCatalog() {
    if (this.selectedProvider) {
      this.selectedProvider.catalogSyncStatus = 0;
      this.providerService
        .removeFromCatalog(this.selectedProvider)
        .subscribe(() => this.getAllProviders());
    }
  }

  getScopes(provider: Provider) {
    this.loadingScope = true;
    this.providerService
      .getScopes(provider.name, provider.version)
      .subscribe((data) => {
        this.loadingScope = false;
        this.scopes = data;
      });
  }

  getResources(provider: Provider) {
    this.providerService
      .getResources(provider.name, provider.version)
      .subscribe((data) => {
        this.resources = data;
      });
  }

  onDeleteScope(provider: Provider, scope: Scope): void {
    this.modalService
      .openModalValidation(
        'The scope ' +
          scope.name +
          ' will be permanently deleted. Are you sure ?\n',
        'Delete the scope',
        'Cancel'
      )
      .onClose.subscribe((result) => {
        if (result === true) {
          this.providerService.deleteScope(provider, scope).subscribe(() => {
            if (this.selectedProvider) {
              this.getScopes(this.selectedProvider);
            }
          });
        }
      });
  }

  getPermissions(provider: Provider) {
    this.providerService
      .getPermissions(provider.name, provider.version)
      .subscribe((data) => {
        this.permissions = data;
      });
  }

  onCreate(): void {
    this.dialogService
      .open(CreateScopeComponent, {
        closeOnBackdropClick: true,
        context: {
          edit: false,
          provider: this.selectedProvider,
          resources: this.resources,
          permissions: this.permissions,
        },
      })
      .onClose.subscribe((scope: Scope | undefined) => {
        if (scope) {
          this.getAllProviders();
          this.scopes.push(scope);
        }
      });
  }

  onEditScope(scope: Scope): void {
    this.subscriptions.add(
      this.dialogService
        .open(CreateScopeComponent, {
          closeOnBackdropClick: true,
          context: {
            edit: true,
            scope: scope,
            provider: this.selectedProvider,
            resources: this.resources,
            permissions: this.permissions,
          },
        })
        .onClose.subscribe((newScope: Scope | undefined) => {
          if (newScope && this.selectedProvider) {
            let index = this.scopes.indexOf(scope);
            this.scopes[index] = newScope;
            this.getScopes(this.selectedProvider);
          }
        })
    );
  }

  onEditProvider(): void {
    this.dialogService
      .open(UpdateProviderComponent, {
        closeOnBackdropClick: true,
        context: {
          provider: this.selectedProvider,
        },
      })
      .onClose.subscribe((provider: Provider | undefined) => {
        if (provider) {
          this.getAllProviders();
        }
      });
  }
}
