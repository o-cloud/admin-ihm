import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { ProviderService } from 'src/app/@core/services/provider.service';
import { Permissions, Provider } from '../../@core/models/provider.model';

@Component({
  selector: 'app-scope-service',
  templateUrl: './update-provider.component.html',
  styleUrls: ['./update-provider.component.scss'],
})
export class UpdateProviderComponent implements OnInit {
  provider: Provider = {
    id: '',
    type: '',
    version: '',
    name: '',
    images: [''],
    description: '',
    metadata: '',
    createdAt: '',
    updatedAt: '',
    registeredAt: '',
    unregisteredAt: '',
    endpointLocation: '',
    workflowStructure: '',
    catalogSyncStatus: 0,
    scopes: [],
  };
  images: string[] = [];
  newImage: string = '';
  permissions = Permissions;
  error: any = {
    All: '',
    Name: '',
    Version: '',
    Description: '',
    Cwl: '',
    Images: '',
  };

  constructor(
    private providerService: ProviderService,
    protected dialogRef: NbDialogRef<UpdateProviderComponent>
  ) {}

  ngOnInit(): void {
    this.images = this.provider.images;
  }

  addImage(): void {
    this.images.push(this.newImage);
    this.provider.images = this.images;
    this.newImage = '';
  }

  removeImage(image: string): void {
    this.images = this.images.filter((item) => item !== image);
    this.provider.images = this.images;
  }

  update(): void {
    this.providerService
      .updateProvider(this.provider.name, this.provider.version, this.provider)
      .subscribe(
        () => {
          this.dialogRef.close(this.provider);
        },
        (err) => {
          err = err.error.errors;
          if (err.hasOwnProperty('422003')) {
            this.error.All = '';
            this.error = err['422003'];
          } else if (err.hasOwnProperty('422004')) {
            this.error.All = err['422004'];
          }
        }
      );
  }

  close(): void {
    this.dialogRef.close();
  }
}
