import {APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import {
  NbCardModule,
  NbIconModule,
  NbLayoutModule,
  NbListModule,
  NbMenuModule,
  NbSidebarModule,
  NbThemeModule,
  NbTooltipModule,
  NbWindowModule,
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxAdminModule } from './@ngx-admin/ngx-admin.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { NetworksModule } from './networks/networks.module';
import { FormsModule } from '@angular/forms';
import { KeycloakAngularModule} from 'keycloak-angular';
import { UsersRoutingModule } from './users/users-routing.module';
import { MonacoEditorModule } from 'ngx-monaco-editor';
import { DataProvidersModule } from './data-providers/data-providers.module';
import { StorageProvidersModule } from './storage-providers/storage-providers.module';
import { ServicesProviderModule } from './services-provider/services-provider.module';
import { ComputingProvidersModule } from './computing-providers/computing-providers.module';
import { CoreModule } from './@core/core.module';
import { ResourcesMonitoringModule } from './resources-monitoring/resources-monitoring.module';
import { DynamicConfigurationService } from './@core/services/dynamic-configuration.service';
import { ProvidersConfigModule } from './providers-config/providers-config.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    KeycloakAngularModule,
    BrowserModule,
    NetworksModule,
    DashboardModule,
    ProvidersConfigModule,
    CoreModule,
    UsersRoutingModule,
    ResourcesMonitoringModule,
    BrowserAnimationsModule,
    NgxAdminModule.forRoot(),
    MonacoEditorModule.forRoot(),
    NbLayoutModule,
    NbEvaIconsModule,
    NbCardModule,
    NbIconModule,
    FormsModule,
    NbListModule,
    NbTooltipModule,
    NbMenuModule.forRoot(),
    NbSidebarModule.forRoot(),
    DataProvidersModule,
    StorageProvidersModule,
    ServicesProviderModule,
    ComputingProvidersModule,
    Ng2SmartTableModule,
    NbWindowModule.forRoot(),
    AppRoutingModule, // Must be the last import
  ],
  providers: [{
    provide: APP_INITIALIZER,
    useFactory: (confService: DynamicConfigurationService) => () => confService.load(),
    multi: true,
    deps: [DynamicConfigurationService] }],

  bootstrap: [AppComponent]
})
export class AppModule {}
