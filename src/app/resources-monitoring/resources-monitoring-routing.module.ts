import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResourcesMonitoringComponent } from './resources-monitoring.component';

const routes: Routes = [
  {
    path: 'resources-monitoring',
    component: ResourcesMonitoringComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResourcesMonitoringRoutingModule {}
