import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ResourcesMonitoringComponent } from './resources-monitoring.component';

describe('ResourcesMonitoringComponent', () => {
  let component: ResourcesMonitoringComponent;
  let fixture: ComponentFixture<ResourcesMonitoringComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResourcesMonitoringComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourcesMonitoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
