import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResourcesMonitoringComponent } from './resources-monitoring.component';
import { SharedModule } from '../@shared/shared.module';
import { ResourcesMonitoringRoutingModule } from './resources-monitoring-routing.module';



@NgModule({
  declarations: [
    ResourcesMonitoringComponent
  ],
  imports: [
    CommonModule,
    ResourcesMonitoringRoutingModule,
    SharedModule,
  ]
})
export class ResourcesMonitoringModule  { }
