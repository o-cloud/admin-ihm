import { Component, OnInit } from '@angular/core';
import { CatalogueService } from '../@core/services/catalogue.service';
import { LocalDataSource } from 'ng2-smart-table';
import { NbDialogService } from '@nebular/theme';
import { CwlEditorButtonViewComponent } from '../@shared/cwl-editor/cwl-editor.component';
import { ProviderType } from '../@core/models/catalog.models';

@Component({
  selector: 'app-storage-providers',
  templateUrl: './storage-providers.component.html',
})
export class StorageProvidersComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();

  settings = {
    noDataMessage: "No provider found...",
    hideSubHeader: true,
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    columns: {
      name: {
        title: 'Name',
        width: '300px',
      },
      version: {
        title: 'Version',
        width: '100px',
      },
      description: {
        title: 'Description',
      },
      metadata: {
        title: 'Metadata',
      },
      workflowStructure: {
        title: 'CWL',
        width: '60px',
        sort: false,
        type: 'custom',
        renderComponent: CwlEditorButtonViewComponent,
      },
    },
  };

  constructor(
    private catalogueService: CatalogueService,
    protected dialogService: NbDialogService,
  ) {}

  ngOnInit(): void {
    this.catalogueService.getLocalProviders(ProviderType.Storage).subscribe((data) => {
      this.source.load(data)
    });
  }

}
