import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { StorageProvidersComponent } from './storage-providers.component';

const routes: Routes = [
  {
    path: 'storage-providers',
    component: StorageProvidersComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StorageProvidersRoutingModule {}
