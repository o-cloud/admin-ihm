import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ServicesProviderComponent } from './services-provider.component';

const routes: Routes = [
  {
    path: 'services-provider',
    component: ServicesProviderComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServicesProviderRoutingModule {}
