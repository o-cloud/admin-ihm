import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicesProviderComponent } from './services-provider.component';
import { ServicesProviderRoutingModule } from './services-provider-routing.module';
import { NbButtonModule, NbCardModule, NbDialogModule, NbFormFieldModule, NbIconModule, NbInputModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { CreateServiceComponent } from './create-service/create-service.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../@shared/shared.module';
import { MonacoEditorModule } from 'ngx-monaco-editor';



@NgModule({
  declarations: [ServicesProviderComponent, CreateServiceComponent],
  imports: [
    CommonModule,
    NbCardModule,
    NbButtonModule,
    Ng2SmartTableModule,
    FormsModule,
    NbInputModule,
    NbIconModule,
    NbFormFieldModule,
    NbDialogModule.forChild(),
    MonacoEditorModule,
    ServicesProviderRoutingModule,
    SharedModule,
  ]
})
export class ServicesProviderModule { }
