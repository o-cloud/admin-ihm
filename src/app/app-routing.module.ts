import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from './@core/guard/admin.guard';

const routes: Routes = [
  {path:'', canActivate:[AdminGuard], children: [
    { path: 'cluster-config', loadChildren: ()=> import('./cluster-config/cluster-config.module').then(m => m.ClusterConfigModule)},
   ]},
  { path: '**', redirectTo: '/dashboard', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
