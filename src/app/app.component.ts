import { Component, OnDestroy, OnInit } from '@angular/core';
import { remove } from 'lodash'
import { NbMenuItem } from '@nebular/theme';
import { KeycloakService } from 'keycloak-angular';
import { environment } from 'src/environments/environment';
import { DiscoveryService } from './@core/services/discovery-service';
import { DynamicConfigurationService } from './@core/services/dynamic-configuration.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  nbOnlinePeers: number = 0
  intervalPeers: any;

  constructor(private discoveryService: DiscoveryService,
    protected readonly keycloak: KeycloakService,
    private confService: DynamicConfigurationService
  ) {}

  async ngOnInit() {
    if(this.confService.authConfiguration.enableKeycloakAuth){
      if (!await this.keycloak.isLoggedIn()) {
        this.keycloak.login()
      }
      if (!this.keycloak.isUserInRole("admin")){
        remove(this.menu, m => m.title === 'Cluster Config')
      }
    }
    this.refreshPeers()
    this.intervalPeers = setInterval(() => {
      this.refreshPeers();
    }, 30000);
  }

  refreshPeers(): void {
    this.discoveryService.getPeers().subscribe((data) => {
      this.menu[1].badge!.text = data.filter(peer => peer.status == 'Online').length.toString()
      this.menu[1].children![1].badge!.text = data.filter(peer => peer.status == 'Online').length.toString()
    });
  }

  title = 'admin-IHM';
  menu: NbMenuItem[] = [
    {
      title: 'Dashboard',
      link: '/dashboard',
      icon: 'home-outline',
    },
    {
      title: 'Networks',
      link: '/networks',
      icon: 'share-outline',
      badge: {
        text: '0',
        status: 'info',
      },
      children: [
        {
          title: 'Overview',
          link: '/networks',
          icon: 'share-outline',
        },
        {
          title: 'Peers',
          link: '/peers',
          icon: 'link-outline',
          badge: {
            text: '0',
            status: 'info',
          },
        },
      ],
    },
    {
      title: 'Local resources',
      icon: 'shopping-bag-outline',
      children: [
        {
          title: 'Data',
          link: '/data-providers',
          icon: 'log-out-outline',
        },
        {
          title: 'Storages',
          link: '/storage-providers',
          icon: 'log-in-outline',
        },
        {
          title: 'Services',
          link: '/services-provider',
          icon: 'sync-outline',
        },
        {
          title: 'Hardware',
          link: '/computing-providers',
          icon: 'flash-outline',
        },
      ],
    },
    {
      title: 'Cluster Config',
      link: '/cluster-config',
      icon: 'folder-outline',
    },
    {
      title: 'Resources Monitoring',
      link: '/resources-monitoring',
      icon: 'folder-outline',
    },
    {
      title: 'Providers Configuration',
      link: '/providers-config',
      icon: 'settings-outline',
    }
  ];

  ngOnDestroy() {
    if (this.intervalPeers) {
      clearInterval(this.intervalPeers);
    }
  }
}
