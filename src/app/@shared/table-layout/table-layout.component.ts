import { Component, HostBinding, Input } from '@angular/core';


@Component({
  selector: 'app-table-layout',
  template: ` 
        <ng-content></ng-content>
  `,
  styleUrls: ['./table.scss']
})
export class TableLayoutComponent {
  
}

