  
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NbDialogRef, NbDialogService } from '@nebular/theme';
import { ViewCell } from 'ng2-smart-table';


@Component({
    template: `
    <nb-card class="modal-container">
        <nb-card-header>CWL</nb-card-header>
        <nb-card-body>
            <div class="editor-div">
                <ngx-monaco-editor class="cwl-editor" [options]="editorOptions" [(ngModel)]="cwl"></ngx-monaco-editor>
            </div>
        </nb-card-body>
        <nb-card-footer>
            <div class="button-wrapper">
                <button nbButton (click)="close()">Cancel</button>
            </div>
        </nb-card-footer>
    </nb-card>
    `,
    styleUrls: ['./cwl-editor.scss'],
})
export class CwlEditorComponent {

    cwl: any;
    editorOptions = {
        theme: 'vs',
        language: 'yaml',
        scrollBeyondLastLine: false,
        domReadOnly: true,
        readOnly: true,
        minimap: {
          enabled: false,
        }
    };

    constructor(
        protected dialogRef: NbDialogRef<CwlEditorComponent>
    ) {}
    
    close(): void {
        this.dialogRef.close();
    }
}

@Component({
    selector: 'button-view',
    template: `
    <div style="text-align: center">
        <button nbButton nbTooltip="See CWL" ghost (click)="onClick()">
            <nb-icon icon="eye-outline"></nb-icon>
        </button>
    </div>
    `,
    styleUrls: ['./cwl-editor.scss'],
  })
export class CwlEditorButtonViewComponent implements ViewCell {

    @Input()
    value!: string | number;
    @Input() rowData: any;

    @Output() save: EventEmitter<any> = new EventEmitter();

    constructor(protected dialogService: NbDialogService) {}

    onClick() {
        this.dialogService.open(CwlEditorComponent, {
            closeOnBackdropClick: true,
            context: { cwl : this.value },
        });
    }
}
