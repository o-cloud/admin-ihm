  
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NbDialogRef, NbDialogService } from '@nebular/theme';
import { ViewCell } from 'ng2-smart-table';


@Component({
    template: `<img [src]="img" class="img-full">`,
    styleUrls: ['./img-preview.scss'],
})
export class ImgPreviewComponent {

    img: string = '';

    constructor(
        protected dialogRef: NbDialogRef<ImgPreviewComponent>
    ) {}
    
    close(): void {
        this.dialogRef.close();
    }
}

@Component({
    selector: 'img-preview',
    template: `<img *ngIf="value != ''" [src]="value" class="img-cell" (click)="onClick()">`,
    styleUrls: ['./img-preview.scss'],
  })
export class ImgPreviewViewComponent implements ViewCell {

    @Input() value!: string;
    @Input() rowData: any;

    constructor(protected dialogService: NbDialogService) {}

    onClick() {
        this.dialogService.open(ImgPreviewComponent, {
            closeOnBackdropClick: true,
            context: { img : this.value },
        });
    }
}
