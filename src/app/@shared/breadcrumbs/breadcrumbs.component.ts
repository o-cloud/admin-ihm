import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-breadcrumbs-item',
  template: ` <a> <ng-content></ng-content></a>`,
  styleUrls: ['breadcrumbs.scss'],
})
export class BreadcrumbsItemComponent {
  @Input() url = '';
}

@Component({
  selector: 'app-breadcrumbs',

  template: ` <ng-content select="app-breadcrumbs-item"></ng-content> `,
  styleUrls: ['breadcrumbs.scss'],
})
export class BreadcrumbsComponent {}
