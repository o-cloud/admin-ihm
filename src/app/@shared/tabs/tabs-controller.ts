import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

export class TabsController<T> {
  // tslint:disable-next-line:variable-name
  private _currentTab: BehaviorSubject<T>;
  private router: Router;
  private subscription = new Subscription();

  constructor(
    route: ActivatedRoute,
    router: Router,
    defaultTab: T,
    isValidTab: (tab: T) => boolean
  ) {
    this._currentTab = new BehaviorSubject<T>(defaultTab);
    this.router = router;
    this.subscription.add(
      route.queryParams.subscribe((params) => {
        const tab = params.tab;
        if (!isValidTab(tab)) {
          this.openTab(defaultTab);
        } else {
          this._currentTab.next(tab);
        }
      })
    );
  }

  teardown(): void {
    this.subscription.unsubscribe();
  }

  currentTab(): Observable<T> {
    return this._currentTab.asObservable();
  }

  openTab(tabName: T, additionalParams: { [key: string]: string } = {}): void {
    if (this._currentTab.getValue() !== tabName) {
      this.router.navigate([], {
        queryParamsHandling: 'merge',
        queryParams: {
          tab: tabName,
          ...additionalParams,
        },
      });
    }
  }
}
