import { Component, HostBinding, Input } from '@angular/core';


@Component({
  selector: 'app-tab',
  template: `
      <button class="tab-link">
        <ng-content></ng-content>
      </button>
  `,
  styleUrls: ['tabs.scss']
})
export class TabComponent {
  @Input() isActive  = false
  @HostBinding('class.tab') tabClass = true
  @HostBinding('class.active') get active(): boolean {
    return this.isActive
  }
}

@Component({
  selector: 'app-tabs-group',
  template: `
    <ng-content select="app-tab"></ng-content>
  `,
  styleUrls: ['tabs.scss']
})

export class TabsGroupComponent {
  @HostBinding('class.tab-group') tabGroupClass = true
}
