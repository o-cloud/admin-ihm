import { Component, HostBinding, Input } from '@angular/core';
import { NbComponentSize } from '@nebular/theme';


@Component({
  selector: 'app-button',
  template: `
      <button nbButton status="basic" [size]="size" [disabled]="disabled" >
        <ng-content></ng-content>
      </button>
  `,
  styleUrls: ['buttons.scss']
})
export class ButtonComponent {
  @Input() disabled = false
  @Input() size: NbComponentSize = "medium"
}

@Component({
  selector: 'app-button-cancel',
  template: `
    <button nbButton status="danger">
        <ng-content></ng-content>
      </button>
  `,
  styleUrls: ['buttons.scss']
})

export class ButtonCancelComponent {
}
