import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { DataProvidersComponent } from './data-providers.component';

const routes: Routes = [
  {
    path: 'data-providers',
    component: DataProvidersComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DataProvidersRoutingModule {}
