import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComputingProvidersComponent } from './computing-providers.component';
import { ComputingProvidersRoutingModule } from './computing-providers-routing.module';
import { NbCardModule, NbSelectModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { SharedModule } from '../@shared/shared.module';
import { ResourcesComponent } from './resources.components';



@NgModule({
  declarations: [ComputingProvidersComponent, ResourcesComponent],
  imports: [
    CommonModule,
    NbCardModule,
    Ng2SmartTableModule,
    NbSelectModule,
    ComputingProvidersRoutingModule,
    SharedModule,
  ]
})
export class ComputingProvidersModule { }
