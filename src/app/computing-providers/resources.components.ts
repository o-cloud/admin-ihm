import { Component, Input, OnInit } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
    template: `
    <div style="display: grid; gap: 5px">
        <div *ngIf="cpu"><strong>CPU :</strong> {{ cpu }}</div>
        <div *ngIf="ram">
            <strong>RAM :</strong> {{ ram }}
            <nb-select [(selected)]="selectedItem" size="small" shape="semi-round" (selectedChange)="selectedChange()">
                <nb-option value="1">Mi</nb-option>
                <nb-option value="2">Gi</nb-option>
            </nb-select>
        </div>
    </div>
    `,
})
export class ResourcesComponent implements ViewCell, OnInit {
    selectedItem = '1';
    cpu: number = -1
    ram: number = -1

    @Input()
    value!: any;
    @Input() rowData: any;

    ngOnInit(): void {
        this.cpu = this.value.resources.cpu ? this.value.resources.cpu : -1;
        this.selectedChange()
    }

    selectedChange() {
        if (this.value.resources.ram) {
            const preRoundRam = this.selectedItem === '1' ? this.value.resources.ram / 100000 : this.value.resources.ram / 100000000;
            this.ram = Math.round(preRoundRam) / 10
        }
    }
}
