import { Component, OnDestroy, OnInit } from '@angular/core';
import { Peer } from 'src/app/@core/models/peer.model';
import { DiscoveryService } from 'src/app/@core/services/discovery-service';
import { IconStatusPeerComponent } from 'src/app/dashboard/icon-status-peer.component';
import { RenderListComponent } from '../components/renderlist.component';

@Component({
  selector: 'app-peers',
  templateUrl: './peers.component.html',
  styleUrls: ['../networks.component.scss'],
})
export class PeersComponent implements OnInit, OnDestroy {
  intervalPeers: any;
  peers: Peer[] = [];

  constructor(private discoveryService: DiscoveryService) {}

  settings = {
    mode: 'external',
    noDataMessage: 'No peer found...',
    columns: {
      name: {
        title: 'Name',
      },
      network: {
        title: 'Network',
      },
      url:{
        title: 'URL',
        type: 'html',
        valuePrepareFunction: (value: string) => `<a href="${value}">${value}</a>`,
      },
      description:{
        title: 'Description',
      },
      categories:{
        title: 'Categories',
        type: 'custom',
        renderComponent: RenderListComponent,
      },
      status: {
        title: 'Status',
        type: 'custom',
        renderComponent: IconStatusPeerComponent,
      },
    },
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    hideSubHeader: true,
  };

  refreshPeers(): void {
    this.discoveryService.getPeers().subscribe((data) => {
      this.peers = data;
    });
  }

  ngOnInit(): void {
    this.refreshPeers();
    this.intervalPeers = setInterval(() => {
      this.refreshPeers();
    }, 30000);
  }

  ngOnDestroy() {
    if (this.intervalPeers) {
      clearInterval(this.intervalPeers);
    }
  }
}
