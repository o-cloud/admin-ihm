import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { ModalsService } from 'src/app/@shared/validation-modal/modals.service';
import { Identity } from '../../@core/models/identity.model';
import { DiscoveryService } from '../../@core/services/discovery-service';
import { CreateIdentityComponent } from './create-identity/create-identity.component';

@Component({
  selector: 'app-identity',
  templateUrl: './identity.component.html',
  styleUrls: ['../networks.component.scss'],
})
export class IdentityComponent {
  @Input() identity: Identity | null = null
  @Output() identityUpdated = new EventEmitter<Identity>();

  constructor(
    private discoveryService: DiscoveryService,
    private modalService: ModalsService,
    protected dialogService: NbDialogService,
  ) {}

  onCreateIdentity(): void {
    this.dialogService.open(CreateIdentityComponent, {
      closeOnBackdropClick: true,
      context: {edit: false},
    }).onClose.subscribe(identity => this.refreshIdentity(identity));
  }

  onEditIdentity(): void {
    this.dialogService.open(CreateIdentityComponent, {
      closeOnBackdropClick: true,
      context: {edit: true},
    }).onClose.subscribe(identity => this.refreshIdentity(identity));
  }

  refreshIdentity(identity: Identity): void {
    if (identity != null) {
      this.identityUpdated.emit(identity);
      this.identity = identity
    }
  }

  onDeleteIdentity(): void {
    this.modalService
      .openModalValidation(
        'Your identity will be permanently deleted \n',
        'Delete my identity',
        'Cancel'
      )
      .onClose.subscribe((result) => {
        if (result === true) {
          this.discoveryService
            .deleteIdentity()
            .subscribe(() => this.identity = null);
        }
      });
  }

}
