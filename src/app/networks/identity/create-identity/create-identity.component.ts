import { Component, OnInit } from '@angular/core';
import { NbDialogRef, NbTagComponent } from '@nebular/theme';
import { Identity } from 'src/app/@core/models/identity.model';
import { DiscoveryService } from 'src/app/@core/services/discovery-service';

@Component({
  selector: 'app-create-identity',
  templateUrl: './create-identity.component.html',
  styleUrls: ['../../networks.component.scss'],
})
export class CreateIdentityComponent implements OnInit {
  categorytoadd = '';
  categorytoadds: Set<string> = new Set([]);
  identity: Identity = {
    name: '',
    url: '',
    description: '',
    categories: [],
    network: '',
    networks: [],
  };
  error = {
    All: '',
    Name: '',
    Url: '',
    Description: '',
    Categories: '',
  };
  edit: any;

  constructor(
    private discoveryService: DiscoveryService,
    protected dialogRef: NbDialogRef<CreateIdentityComponent>
  ) {}

  ngOnInit(): void {
    if (this.edit) {
      this.discoveryService.getIdentity().subscribe((data) => {
        this.identity = data;
        this.identity.categories.forEach((category) =>
          this.categorytoadds.add(category)
        );
      });
    }
  }

  create(): void {
    this.identity.categories = Array.from(this.categorytoadds);
    this.discoveryService.addIdentity(this.identity).subscribe(
      (data) => {
        this.dialogRef.close(data);
      },
      (err) => {
        err = err.error.errors;
        if (err.hasOwnProperty('422003')) {
          this.error.All = '';
          this.error = err['422003'];
        } else if (err.hasOwnProperty('422004')) {
          this.error.All = err['422004'];
        }
      }
    );
  }

  update(): void {
    this.identity.categories = Array.from(this.categorytoadds);
    this.discoveryService
      .updateIdentity(this.identity)
      .subscribe(
        (data) => {
          this.dialogRef.close(data);
        },
        (err) => {
          err = err.error.errors;
          if (err.hasOwnProperty("422003")) {
            this.error.All = "";
            this.error = err["422003"];
          } else if (err.hasOwnProperty("422004")) {
            this.error.All = err["422004"];
          }
        }
      );
  }

  close(): void {
    this.dialogRef.close();
  }

  onTagRemove(tagToRemove: NbTagComponent): void {
    this.categorytoadds.delete(tagToRemove.text);
  }

  onTagAdd(): void {
    if (this.categorytoadd) {
      this.categorytoadds.add(this.categorytoadd);
    }
    this.categorytoadd = '';
  }
}
