import { Directive, Input, OnInit } from "@angular/core";
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from "@angular/forms";
import { Network } from "src/app/@core/models/network.model";
import { DiscoveryService } from "src/app/@core/services/discovery-service";

@Directive({
    selector: '[networkForbiddenName]',
    providers: [{provide: NG_VALIDATORS, useExisting: ForbiddenNameValidatorDirective, multi: true}]
})
export class ForbiddenNameValidatorDirective implements Validator, OnInit {
    networks: Network[] = [];

    constructor(
      private discoveryService: DiscoveryService,
    ) {}

    ngOnInit(): void {
      this.discoveryService.getNetworks().subscribe((data) => {
        this.networks = data;
      });
    }

    validate(control: AbstractControl): ValidationErrors | null {
        return this.networks.length ? this.forbiddenNameValidator(this.networks)(control) : null;
    }

    forbiddenNameValidator(networks: Network[]): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
          const forbidden = networks.filter(element => element.name == control.value).length
          return forbidden ? {forbiddenName: {value: 'Network name already exists on cluster'}} : null;
        };
    }
}
