import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { NbDialogRef, NbTagComponent } from '@nebular/theme';
import { entrypoint, Network } from 'src/app/@core/models/network.model';
import { DiscoveryService } from 'src/app/@core/services/discovery-service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create-network',
  templateUrl: './create-network.component.html',
  animations: [
    trigger(
      'inOutAnimation', 
      [
        transition(
          ':enter', 
          [
            style({ height: 0, opacity: 0 }),
            animate('0.3s ease-out', 
                    style({ height: 60, opacity: 1 }))
          ]
        ),
        transition(
          ':leave', 
          [
            style({ height: 60, opacity: 1 }),
            animate('0.3s ease-in', 
                    style({ height: 0, opacity: 0 }))
          ]
        )
      ]
    )
  ],
  styleUrls: ['../networks.component.scss'],
})
export class CreateNetworkComponent implements OnInit {
  entrypoints: entrypoint[] = environment.ipfsEntrypoints;
  joiningnodetoadd = '';
  joiningnodestoadds: Set<string> = new Set([]);
  creationtype = 'Create';
  isDefaultExist = false;
  is_private = true;
  network: Network = {
    name: '',
    is_private: true,
    private_key: undefined,
    joining_nodes: undefined,
    entrypoint: '',
  };
  error = {
    All: '',
    Name: '',
    PrivateKey: '',
    Entrypoint: '',
    Joiningnodes: '',
  };
  datasource: any;

  constructor(
    private discoveryService: DiscoveryService,
    protected dialogRef: NbDialogRef<CreateNetworkComponent>
  ) {}

  ngOnInit(): void {
    const existingNetwork = <Network[]>(this.datasource.data)
    const usedEntrypoints = existingNetwork.map(network => network.entrypoint)
    this.entrypoints = this.entrypoints.filter(entrypoint => !usedEntrypoints.includes(entrypoint.name))
    this.isDefaultExist = (existingNetwork.filter(network => !network.is_private).length != 0)
    this.network.is_private = this.is_private

    if (!this.is_private) {
      this.network.name = "public-network"
      this.network.entrypoint = "ipfs"
    } else {
      this.entrypoints = this.entrypoints.filter(entrypoint => entrypoint.name != "ipfs")
    }
  }

  create(): void {
    if (this.network.is_private) {
      if (this.creationtype == 'Create') {
        this.network.joining_nodes = undefined
      } else {
        if (this.joiningnodestoadds.size) {
          this.network.joining_nodes = Array.from(this.joiningnodestoadds);
        } else {
          this.error.Joiningnodes = 'Joining nodes are required for joining Network'
          return
        }
      }
    } else {
      this.network.private_key = ''
      this.network.joining_nodes = undefined
    }
    this.discoveryService.addNetwork(this.network).subscribe(
      () => {
        if(this.network.is_private) {
          this.datasource.add(this.network);
          this.datasource.refresh();
        }
        this.close(this.network);
      },
      (err) => {
        err = err.error.errors;
        if (err.hasOwnProperty('422001')) {
          if (err['422001'] instanceof Object) {
            this.error.All = '';
            this.error = err['422001'];
          } else {
            this.error.All = err['422001'];
          }
        } else if (err.hasOwnProperty('422002')) {
          this.error.All = err['422002'];
        }
      }
    );
  }

  close(network: Network | null = null): void {
    this.dialogRef.close(network);
  }

  onTagRemove(tagToRemove: NbTagComponent): void {
    this.joiningnodestoadds.delete(tagToRemove.text);
  }

  onTagAdd(): void {
    if (this.joiningnodetoadd) {
      this.joiningnodestoadds.add(this.joiningnodetoadd);
    }
    this.joiningnodetoadd = '';
  }
}
