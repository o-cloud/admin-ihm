import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { ViewCell } from 'ng2-smart-table';
import { DiscoveryService } from 'src/app/@core/services/discovery-service';
import { ConnectionInfoDialogComponent } from './connection-info-dialog.component';

@Component({
    template: `
    <div *ngIf="rowData.ready" style="text-align: center">
        <button nbButton nbTooltip="Connection data" ghost (click)="getInformations()"><nb-icon icon="log-in-outline"></nb-icon></button>
        <button nbButton nbTooltip="Delete" ghost (click)="onDelete()"><nb-icon icon="trash-2-outline" status="danger"></nb-icon></button>
    </div>
    <div *ngIf="rowData.notready" style="position: inherit" [nbSpinner]="true" nbSpinnerMessage="Creating..." nbSpinnerStatus="info"></div>
    <div *ngIf="rowData.pending" style="position: inherit" [nbSpinner]="true" nbSpinnerMessage="Creating..." nbSpinnerStatus="success"></div>
    <div *ngIf="rowData.deleting" style="position: inherit" [nbSpinner]="true" nbSpinnerMessage="Deleting..." nbSpinnerStatus="danger"></div>
    `,
    styleUrls: ['../networks.component.scss'],
})
export class NetworkActionsComponent implements ViewCell {
    constructor(
        private discoveryService: DiscoveryService,
        protected dialogService: NbDialogService,
    ) {}

    @Input()
    value!: string | number;
    @Input() rowData: any;

    @Output() delete: EventEmitter<any> = new EventEmitter();

    getInformations() {
        this.discoveryService.getNetworksConnectionInfo(this.rowData.name).subscribe((data) => {
            this.dialogService.open(ConnectionInfoDialogComponent, {
                closeOnBackdropClick: true,
                context: {
                    private_key: data.private_key != "" ? data.private_key : "No information",
                    peer_id: data.peer_id != null ? data.peer_id[0] : "No information"
                },
            });
        });
    }

    onDelete(): void {
        this.delete.emit(this.rowData);
    }
}
