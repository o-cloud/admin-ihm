  
import { Component } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';


@Component({
    template: `
    <nb-card>
        <nb-card-header>
            Connection informations
        </nb-card-header>
        <nb-list>
            <nb-list-item style="display: inline-grid">
                <h2 style="line-height: 0">Private key</h2>
                <p>{{ private_key }}</p>
            </nb-list-item>
            <nb-list-item style="display: inline-grid">
                <h2 style="line-height: 0">Peer ID</h2>
                <p>{{ peer_id }}</p>
            </nb-list-item>
            <nb-list-item style="justify-content: center">
                <button nbButton (click)="close()">Cancel</button>
            </nb-list-item>
        </nb-list>
    </nb-card>
    `,
    styleUrls: ['../networks.component.scss'],
})
export class ConnectionInfoDialogComponent {

    private_key: any;
    peer_id: any;

    constructor(
        protected dialogRef: NbDialogRef<ConnectionInfoDialogComponent>
    ) {}
    
    close(): void {
        this.dialogRef.close();
    }
}