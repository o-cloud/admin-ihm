  
import { Component, Input } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
    selector: "nebular-list",
    template: `
    <nb-tag-list>
        <nb-tag 
            appearance="outline"
            status="basic"
            style="color: black; font-weight: 400"
            *ngFor="let element of value"
            [text]="element"
        ></nb-tag>
    </nb-tag-list>
    `,
    styleUrls: ['../networks.component.scss'],
})
export class RenderListComponent implements ViewCell {

  @Input()
  value!: string | number | any;
  @Input() rowData: any;

}
