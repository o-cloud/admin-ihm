import { Component, Input } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
    selector: "status-icon",
    template: `
    <div style="text-align: center">
        <div 
            *ngIf="value;
            then ok;
            else notok"></div>
        <ng-template #ok>
            <nb-icon
                style="font-size: 1.4rem"
                icon="checkmark-circle-2-outline"
                status="success"></nb-icon>
        </ng-template>
        <ng-template #notok>
            <nb-icon
                style="font-size: 1.4rem"
                icon="alert-circle-outline"
                status="danger"></nb-icon>
        </ng-template>
    </div>
    `,
})
export class IconStatusComponent implements ViewCell {

    @Input()
    value!: string | number;
    @Input() rowData: any;

}
