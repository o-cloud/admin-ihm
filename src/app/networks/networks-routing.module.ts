import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NetworksComponent } from './networks.component';
import { PeersComponent } from './peers/peers.component';

const routes: Routes = [
  {
    path: 'networks',
    component: NetworksComponent,
  },
  {
    path: 'peers',
    component: PeersComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NetworksRoutingModule {}
