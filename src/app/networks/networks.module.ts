import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  NbButtonModule,
  NbCardModule,
  NbDialogModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbRadioModule,
  NbSelectModule,
  NbSpinnerModule,
  NbTagModule,
  NbToggleModule,
  NbTooltipModule,
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ConnectionInfoDialogComponent } from './components/connection-info-dialog.component';
import { CreateNetworkComponent } from './create-network/create-network.component';
import { ForbiddenNameValidatorDirective } from './create-network/network-name.directive';
import { CreateIdentityComponent } from './identity/create-identity/create-identity.component';
import { NetworksRoutingModule } from './networks-routing.module';
import { NetworksComponent } from './networks.component';
import { PeersComponent } from './peers/peers.component';
import { RenderListComponent } from './components/renderlist.component';
import { IconStatusComponent } from './components/status.component';
import { NetworkActionsComponent } from './components/networks-actions.component';
import { IdentityComponent } from './identity/identity.component';
import { SharedModule } from '../@shared/shared.module';

@NgModule({
  declarations: [
    NetworksComponent,
    PeersComponent,
    CreateIdentityComponent,
    CreateNetworkComponent,
    NetworkActionsComponent,
    ConnectionInfoDialogComponent,
    IconStatusComponent,
    ForbiddenNameValidatorDirective,
    RenderListComponent,
    IdentityComponent,
  ],
  imports: [
    NetworksRoutingModule,
    CommonModule,
    NbCardModule,
    NbIconModule,
    Ng2SmartTableModule,
    NbTagModule,
    NbInputModule,
    NbFormFieldModule,
    FormsModule,
    NbListModule,
    NbToggleModule,
    NbSelectModule,
    NbRadioModule,
    NbTooltipModule,
    NbSpinnerModule,
    NbDialogModule.forChild(),
    NbButtonModule,
    SharedModule,
  ],
  exports: [
    NetworksComponent,
    PeersComponent,
    CreateIdentityComponent,
    CreateNetworkComponent,
    NetworkActionsComponent,
    ConnectionInfoDialogComponent,
    IconStatusComponent,
    ForbiddenNameValidatorDirective,
    RenderListComponent,
    IdentityComponent,
    NetworksRoutingModule,
    CommonModule,
    NbCardModule,
    NbIconModule,
    Ng2SmartTableModule,
    NbTagModule,
    NbInputModule,
    NbFormFieldModule,
    FormsModule,
    NbListModule,
    NbToggleModule,
    NbSelectModule,
    NbRadioModule,
    NbTooltipModule,
    NbDialogModule,
    NbButtonModule,
    NbSpinnerModule,
  ],
})
export class NetworksModule {}
