import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { LocalDataSource } from 'ng2-smart-table';
import { Identity } from '../@core/models/identity.model';
import { DiscoveryService } from '../@core/services/discovery-service';
import { CreateNetworkComponent } from './create-network/create-network.component';
import { RenderListComponent } from './components/renderlist.component';
import { IconStatusComponent } from './components/status.component';
import { NetworkActionsComponent } from './components/networks-actions.component';
import { entrypoint, Network, NetworkState } from '../@core/models/network.model';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { ModalsService } from '../@shared/validation-modal/modals.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-networks',
  templateUrl: './networks.component.html',
  styleUrls: ['./networks.component.scss'],
})
export class NetworksComponent implements OnInit, OnDestroy {
  entrypoints: entrypoint[] = environment.ipfsEntrypoints;
  identity: Identity | null = null
  sourceNetwork: LocalDataSource = new LocalDataSource();
  nbOnlinePeers: number = 0
  intervalStatus: any;
  intervalPeers: any;
  default_network: any | null = null;

  constructor(
    private router: Router,
    private discoveryService: DiscoveryService,
    private modalService: ModalsService,
    protected dialogService: NbDialogService,
  ) {}

  settingsNetwork = {
    hideSubHeader: true,
    mode: 'external',
    noDataMessage: 'No network found...',
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    columns: {
      name: {
        title: 'Name',
        type: 'html',
        width: '300px',
        valuePrepareFunction: (name: string, rowdata: any) => {
          return rowdata.is_private ? name : name + '<strong> (default)</strong>';
        },
      },
      is_private: {
        hide: true,
        sortDirection: 'asc',
      },
      joining_nodes: {
        title: 'Joining nodes',
        type: 'custom',
        renderComponent: RenderListComponent,
      },
      entrypoint: {
        title: 'Entrypoint',
        width: '110px',
        valuePrepareFunction: (name: string) => {
          return this.getPortFromEntrypoints(name);
        },
      },
      published: {
        title: 'Identity',
        type: 'custom',
        sort: false,
        width: '90px',
        renderComponent: IconStatusComponent,
      },
      ready: {
        title: 'Ready',
        type: 'custom',
        sort: false,
        width: '90px',
        renderComponent: IconStatusComponent,
      },
      _custom_actions: {
        title: '',
        width: '130px',
        sort: false,
        type: 'custom',
        renderComponent: NetworkActionsComponent,
        onComponentInitFunction : this.onNetworkAction.bind(this),
      },
    },
  };

  ngOnInit(): void { 
    this.discoveryService.getIdentity().subscribe(
    (data) => this.identity = data,
    (err: HttpErrorResponse) => {
      if(err.status != 404) {
        throwError(err)
      }
    });
    this.discoveryService.getNetworks().subscribe((data) => {
      const private_network = data.filter(el => el.is_private)
      this.default_network = data.filter(el => !el.is_private)[0]
      this.sourceNetwork.load(private_network).then(() => {
        this.refreshStatus()
      });
    });
    this.refreshPeers();

    this.intervalStatus = setInterval(() => {
      this.refreshStatus();
    }, 10000);

    this.intervalPeers = setInterval(() => {
      this.refreshPeers();
    }, 30000);
  }

  refreshStatus(network: Network | null = null): void {
    this.refreshPrivateStatus();
    this.refreshDefaultStatus(network);
  }

  refreshDefaultStatus(network: Network | null = null): void {
    if (network != null && !network.is_private) {
      this.default_network = network
    }
    this.discoveryService.getNetworksStatus(this.default_network.name).subscribe(
      (data) => {
        this.default_network.ready = data.state == NetworkState.Ready;
        this.default_network.pending = data.state == NetworkState.Pending;
        this.default_network.deleting = data.state == NetworkState.Deleting;
        this.default_network.notready = data.state == NetworkState.NotReady;
        this.default_network.published = data.is_identity_published;
      },
      (err: HttpErrorResponse) => {
        if(err.status == 404) {
          this.default_network = null
        }
      }
    );
  }

  refreshPrivateStatus(): void {
    this.sourceNetwork.getAll().then(results => {
      results.forEach((element: any) => {
        this.discoveryService.getNetworksStatus(element.name).subscribe(
          (data) => {
            const updated = Object.assign({}, element)
            updated.ready = data.state == NetworkState.Ready;
            updated.pending = data.state == NetworkState.Pending;
            updated.deleting = data.state == NetworkState.Deleting;
            updated.notready = data.state == NetworkState.NotReady;
            updated.published = data.is_identity_published;
            this.sourceNetwork.update(element, updated)
          },
          (err: HttpErrorResponse) => {
            if(err.status == 404) {
              this.sourceNetwork.remove(element)
            }
          }
        );
      })
    })
  }

  refreshPeers(): void {
    this.discoveryService.getPeers().subscribe((data) => {
      this.nbOnlinePeers = data.filter(peer => peer.status == 'Online').length
    });
  }

  onNetworkAction(instance: any) {
    instance.delete.subscribe((row: any) => this.onDelete(row));
  }

  onIdentityUpdated(identity: Identity) {
    this.identity = identity;
  }

  onCreate(is_private: boolean): void {
    this.dialogService.open(CreateNetworkComponent, {
      closeOnBackdropClick: true,
      context: { datasource: this.sourceNetwork, is_private: is_private },
    }).onClose.subscribe((result) => result != null ? this.refreshStatus(result) : null)
  }

  onDelete(row: any): void {
    this.modalService
      .openModalValidation(
        'The network ' + row.name + ' will be permanently deleted. Are you sure ?\n',
        'Delete the network',
        'Cancel'
      )
      .onClose.subscribe((result) => {
        if (result === true) {
          this.discoveryService
            .deleteNetwork(row.name)
            .subscribe(() => {
              this.refreshStatus();
            });
        }
      });
  }

  goToPage(pageName: string): void {
    this.router.navigate([`${pageName}`]);
  }

  getPortFromEntrypoints(name: string): number {
    const entrypoint = this.entrypoints.find(e => e.name == name)
    return entrypoint != undefined ? entrypoint.port : -1;
  }

  ngOnDestroy() {
      if (this.intervalStatus) {
        clearInterval(this.intervalStatus);
      }
      if (this.intervalPeers) {
        clearInterval(this.intervalPeers);
      }
  }

  getNetworksNumber(): number {
    const private_network_count = this.default_network != null ? 1 : 0
    return this.sourceNetwork.count() + private_network_count
  }
}
