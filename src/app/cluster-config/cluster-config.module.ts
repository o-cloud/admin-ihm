import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ClusterConfigComponent } from './components/main/cluster-config.component';
import { ClusterRoutingModule } from './cluster-config-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ReleaseDetailComponent } from './components/release-detail/release-detail.component';
import { ReleaseCreateComponent } from './components/release-create/release-create.component';
import {
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbDialogModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbOptionModule,
  NbSelectComponent,
  NbSelectModule,
  NbTagModule,
  NbTooltipModule,
} from '@nebular/theme';
import { SharedModule } from '../@shared/shared.module';



@NgModule({
  declarations: [
    ClusterConfigComponent,
    ReleaseDetailComponent,
    ReleaseCreateComponent
  ],
  imports: [
    Ng2SmartTableModule,
    ClusterRoutingModule,
    FormsModule,
    NbAlertModule,
    NbTooltipModule,
    NbSelectModule,
    NbButtonModule,
    NbCardModule,
    NbDialogModule,
    NbFormFieldModule,
    NbIconModule,
    NbInputModule,
    NbListModule,
    NbOptionModule,
    NbTagModule,
    CommonModule,
    SharedModule,
  ],
})
export class ClusterConfigModule { }
