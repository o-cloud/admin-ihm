import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';
import { switchMap } from 'rxjs/operators';
import { Chart } from 'src/app/@core/models/cluster-config.models';
import { CoreManagerService } from 'src/app/@core/services/core-manager.service';

@Component({
  selector: 'app-release-create',
  templateUrl: './release-create.component.html',
  styleUrls: ['./release-create.component.scss'],
})
export class ReleaseCreateComponent implements OnInit {
  ready = 0;
  chartName: string = 'invalid';
  chartDetail!: Chart;
  error?: string;
  releaseName: string = 'releasename';

  selectedVersion = '';
  availableVersions: string[] = [];

  completeconf: string = '';
  jsonconf: string = '{}';

  constructor(
    protected dialogRef: NbDialogRef<ReleaseCreateComponent>,
    private svc: CoreManagerService
  ) {}

  ngOnInit(): void {
    this.releaseName = this.chartName;
    this.loadChart();
    this.setupVersions();
  }

  loadChart(): void {
    this.svc.getChartDetail(this.chartName).subscribe((cdetail) => {
      this.chartDetail = cdetail;
      this.selectedVersion = this.chartDetail.metadata.version;
      this.completeconf = JSON.stringify(cdetail.values, null, 4);
      this.ready++;
    });
  }

  setupVersions(): void {
    this.svc.getChartVersions(this.chartName).subscribe((versions) => {
      versions.forEach((v) => this.availableVersions.push(v.version));
      this.ready++;
    });
  }

  updateConfig(): void {
    try {
      let deployParam = {
        name: this.releaseName,
        namespace: '',
        chartName: this.chartName,
        chartVersion: this.selectedVersion,
        flags: {},
        config: JSON.parse(this.jsonconf),
      };
      this.svc.createRelease(deployParam).subscribe(
        () => this.dialogRef.close(),
        (err) => (this.error = err.error.error)
      );
    } catch (err) {
      this.error = '' + err;
    }
  }
}
