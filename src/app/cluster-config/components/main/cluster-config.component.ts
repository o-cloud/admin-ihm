import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { LocalDataSource } from 'ng2-smart-table';
import { timeout } from 'rxjs-compat/operator/timeout';
import {
  Chart,
  Metadata,
  SimpleRelease,
} from 'src/app/@core/models/cluster-config.models';
import { CoreManagerService } from 'src/app/@core/services/core-manager.service';
import { ReleaseCreateComponent } from '../release-create/release-create.component';

@Component({
  selector: 'app-cluster-config',
  templateUrl: './cluster-config.component.html',
  styleUrls: ['./cluster-config.component.scss'],
})
export class ClusterConfigComponent implements OnInit {
  umbrellamode: boolean = false;
  releases: LocalDataSource = new LocalDataSource();
  charts: LocalDataSource = new LocalDataSource();
  constructor(
    private svc: CoreManagerService,
    private router: Router,
    private route: ActivatedRoute,
    protected dialogService: NbDialogService
  ) {}

  ngOnInit(): void {
    this.refresh();
  }

  refresh(): void {
    this.releases.empty();
    this.charts.empty();
    this.svc.getDeployedModules().subscribe((rel) => this.fillReleases(rel));
    this.svc.getChartList().subscribe((charts) => this.fillCharts(charts));
  }

  fillReleases(rel: SimpleRelease[]): void {
    let r: any[] = [];
    rel.forEach((element) => {
      let conv = {
        releasename: element.name,
        releasenumber: element.version,
        chartname: element.metadata.name,
        chartversion: element.metadata.version,
      };
      r.push(conv);
    });
    this.releases.load(r);
  }
  releaseEdit(event: any): void {
    this.router.navigate(['release', event.data.releasename], {
      relativeTo: this.route,
    });
  }

  fillCharts(rel: Metadata[]): void {
    let r: any[] = [];
    rel.forEach((element) => {
      let conv = {
        chartname: element.name,
        chartversion: element.version,
        description: element.description,
      };
      r.push(conv);
    });
    this.charts.load(r);
    this.checkVersionRelease();
  }

  async checkVersionRelease() {
    let rels = await this.releases.getAll();
    let chas = await this.charts.getAll();
    let mapcharts = new Map();
    chas.forEach((element: any) => {
      mapcharts.set(element.chartname, element.chartversion);
    });
    console.log(mapcharts);
    rels.forEach((el: any) => {
      if (el.chartversion != mapcharts.get(el.chartname)) {
        el.chartversion += ' * New version available';
      }
    });
    this.releases.refresh();
  }

  createChart(event: any): void {
    this.dialogService
      .open(ReleaseCreateComponent, {
        closeOnBackdropClick: true,
        context: { chartName: event.data.chartname },
      })
      .onClose.subscribe(() => {
        this.refresh();
      });
  }

  //Tables settings

  settingsDeployedTable = {
    hideSubHeader: true,
    mode: 'external',
    noDataMessage: 'No release found',
    actions: {
      add: false,
      edit: true,
      delete: false,
      position: 'right',
      columnTitle: '',
    },
    add: {
      addButtonContent:
        '<img src="assets/images/icons/plus-square-outline.svg" width="20" height="20">',
      confirmCreate: true,
    },
    edit: {
      editButtonContent:
        '<img src="assets/images/icons/edit-2-outline.svg" width="20" height="20">',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent:
        '<img src="assets/images/icons/trash-2-outline.svg" width="20" height="20">',
      confirmDelete: true,
    },
    columns: {
      releasename: { title: 'Release Name' },
      releasenumber: { title: 'Release Number' },
      chartname: { title: 'Chart Name' },
      chartversion: { title: 'Chart Version' },
    },
  };

  settingsAvailableTable = {
    hideSubHeader: true,
    mode: 'external',
    noDataMessage: 'No release found',
    actions: {
      add: false,
      edit: true,
      delete: false,
      position: 'right',
      columnTitle: '',
    },
    edit: {
      editButtonContent:
        '<img src="assets/images/icons/green-plus.svg" width="20" height="20">',
    },
    columns: {
      chartname: { title: 'Chart Name' },
      chartversion: { title: 'Chart Version' },
      description: { title: 'Description', width: '60%' },
    },
  };
}
