import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Chart, Release } from 'src/app/@core/models/cluster-config.models';
import { CoreManagerService } from 'src/app/@core/services/core-manager.service';
import { ModalsService } from 'src/app/@shared/validation-modal/modals.service';

@Component({
  selector: 'app-release-detail',
  templateUrl: './release-detail.component.html',
  styleUrls: ['./release-detail.component.scss'],
})
export class ReleaseDetailComponent implements OnInit {
  alertVersion = false;
  upgradingVersion = false;
  selectedVersion = "";
  error?: string;
  availableVersions: string[] = [];
  releaseName: string = 'invalid';
  releaseDetail!: Release;
  chartDetail!: Chart;
  completeconf: string = '';
  jsonconf: string = '';
  constructor(
    private modalService: ModalsService,
    private svc: CoreManagerService,
    private location: Location,
    private route: ActivatedRoute) {}

  ngOnInit(): void {

    this.releaseName = this.route.snapshot.paramMap.get('releaseName') || 'invalid';
    this.svc
      .getReleaseDetail(this.releaseName)
      .subscribe((rel) => this.applyReleaseDetail(rel));
  }

  applyReleaseDetail(detail: Release): void {
    this.releaseDetail = detail;
    this.selectedVersion = this.releaseDetail.chart.metadata.version
    this.availableVersions.push(this.selectedVersion)
    this.jsonconf = JSON.stringify(this.releaseDetail.config || {}, null, 4);
    this.loadChart();
  }

  loadChart(): void {
    this.svc
      .getChartDetail(this.releaseDetail.chart.metadata.name)
      .subscribe((cdetail) => {
        this.chartDetail = cdetail;
        this.setupVersions()
        this.alertVersion = this.chartDetail.metadata.version != this.releaseDetail.chart.metadata.version
        this.completeconf = JSON.stringify(cdetail.values, null, 4);
      });
  }

  setupVersions(): void {
    this.svc.getChartVersions(this.releaseDetail.chart.metadata.name)
    .subscribe(versions => {versions.forEach(v=>this.availableVersions.push(v.version))})

  }

  changeVersion(): void {
    this.upgradingVersion = true
  }

  updateConfig(): void {
    try {
      let newConf = JSON.parse(this.jsonconf);
      this.svc.updateRelease(this.releaseName, newConf, this.upgradingVersion?this.selectedVersion:undefined)
        .subscribe(console.log, err => this.error = ""+err.error.error);
    } catch (err) {
      console.log(""+err);
      this.error =""+err;
    }

  }


  deleteRelease(): void {
    this.modalService
      .openModalValidation(
        `Delete ${this.releaseName} from cluster ?`,
        'Yes',
        'No'
      )
      .onClose.subscribe((result) => {
        if (result === true) {
          this.svc
            .deleteRelease(this.releaseName)
            .subscribe(() => {
                this.location.back()
            });
        }
      });
  }
}
