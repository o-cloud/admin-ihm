import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClusterConfigComponent } from './components/main/cluster-config.component';
import { ReleaseDetailComponent } from './components/release-detail/release-detail.component';

const routes: Routes = [
  {path: 'release/:releaseName',component: ReleaseDetailComponent,},
  {path: '',component: ClusterConfigComponent,},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClusterRoutingModule {}
