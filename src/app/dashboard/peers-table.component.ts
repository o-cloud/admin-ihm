import {Component, Input, OnInit} from '@angular/core';
import {IconStatusPeerComponent} from './icon-status-peer.component';
import {Peer} from '../@core/models/peer.model';

@Component({
  selector: 'app-peers-table',
  template: `
  <app-table-layout>
    <ng2-smart-table
      [settings]="settings"
      [source]="peers"
    ></ng2-smart-table>
  </app-table-layout>
  `
})

export class PeersTableComponent implements OnInit {

  @Input() peers: Peer[] = [];

  settings = {
    mode: 'external',
    columns: {
      name: {
        title: 'Name',
      },
      network: {
        title: 'Network',
      },
      status: {
        title: 'Status',
        type: 'custom',
        renderComponent: IconStatusPeerComponent,
      },
    },
    actions: {
      add: false,
      edit: false,
      delete: false,
      positon: 'right',
      columnTitle: '',
    },
    hideSubHeader: true,
  };

  constructor() {
  }

  ngOnInit(): void {
  }
}
