import {AfterViewInit, Component, ElementRef, HostListener, Input, OnInit, ViewChild} from '@angular/core';
import * as shape from 'd3-shape';
import * as _ from 'lodash';

@Component({
  selector: 'app-chart-component',
  template: `
    <div style="width: 100%; height: 100%;" #chartcontainer>
      <ngx-charts-line-chart [view]="lineChartView" [scheme]="lineChartColorScheme" [results]="data"
                             [gradient]="lineChartGradient" [xAxis]="lineChartShowXAxis" [yAxis]="lineChartShowYAxis"
                             [legend]="lineChartShowLegend" [showXAxisLabel]="lineChartShowXAxisLabel"
                             [showYAxisLabel]="lineChartShowYAxisLabel" [xAxisLabel]="lineChartXAxisLabel"
                             [yAxisLabel]="lineChartYAxisLabel" [autoScale]="lineChartAutoScale"
                             [curve]="lineChartLineInterpolation"
                             >
      </ngx-charts-line-chart>
    </div>
  `
})

export class ChartComponent implements OnInit, AfterViewInit {
  @Input() data: any = [];
  @ViewChild('chartcontainer') chartcontainer!: ElementRef<HTMLElement>;
  lineChartShowXAxis = true;
  lineChartShowYAxis = true;
  lineChartGradient = false;
  lineChartShowLegend = false;
  lineChartShowXAxisLabel = true;
  lineChartXAxisLabel = 'MOIS';
  lineChartShowYAxisLabel = true;
  lineChartYAxisLabel = 'EUR';
  lineChartColorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5'],
  };
  lineChartAutoScale = true;
  lineChartLineInterpolation = shape.curveBasis;
  lineChartView: [number, number] = [350, 400];

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    setTimeout(this.resizeChart, 10);
  }

  resizeChart = (): void => {
    const boundig = this.chartcontainer.nativeElement.getBoundingClientRect();
    this.lineChartView = [boundig.width, boundig.height];
  }

  // tslint:disable
  resizecharttrottle = _.throttle(this.resizeChart, 500);

  @HostListener('window:resize', ['$event'])
  onResize(): void {
    this.resizecharttrottle();
  }
}
