import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashBoardRoutingModule } from './dashboard-routing.module';
import {
  NbCardModule,
  NbButtonModule,
  NbSelectModule,
  NbIconModule,
} from '@nebular/theme';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { IconStatusPeerComponent } from './icon-status-peer.component';
import {ChartComponent} from './chart.component';
import {PeersTableComponent} from './peers-table.component';
import { NetworksModule } from '../networks/networks.module';
import { SharedModule } from '../@shared/shared.module';

@NgModule({
  declarations: [DashboardComponent, IconStatusPeerComponent, ChartComponent, PeersTableComponent],
  imports: [
    CommonModule,
    DashBoardRoutingModule,
    NbButtonModule,
    NbCardModule,
    NgxChartsModule,
    Ng2SmartTableModule,
    NbIconModule,
    NbSelectModule,
    NetworksModule,
    SharedModule,
  ],
})
export class DashboardModule {}
