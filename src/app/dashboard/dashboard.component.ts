import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { Identity } from '../@core/models/identity.model';
import { Peer } from '../@core/models/peer.model';
import { DiscoveryService } from '../@core/services/discovery-service';
import { multi } from './data';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  constructor(
    private router: Router,
    private discoverservice: DiscoveryService
  ) {
    Object.assign(this, { multi });
  }

  // multi= this.kubecostService.getCostByCluster(this.windowPar);
  multi = multi;
  peers: Peer[] = [];
  identity: Identity | null = null;

  ngOnInit(): void {
    this.discoverservice.getPeers().subscribe((data) => {
      this.peers = data;
    });
    this.discoverservice.getIdentity().subscribe(
      (data) => this.identity = data,
      (err: HttpErrorResponse) => {
        if(err.status != 404) {
          throwError(err)
        }
      }
    );
  }

  onIdentityUpdated(identity: Identity) {
    this.identity = identity;
  }

  goToPage(pageName: string): void {
    this.router.navigate([`${pageName}`]);
  }
}
