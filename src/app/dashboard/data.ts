export let multi = [
  {
    name: 'Cluster1',
    series: [
      {
        name: 'Jan',
        value: 700,
      },
      {
        name: 'Feb',
        value: 750,
      },
      {
        name: 'Mar',
        value: 775,
      },
      {
        name: 'Apr',
        value: 725,
      },
      {
        name: 'May',
        value: 750,
      },
      {
        name: 'Jun',
        value: 800,
      },
      {
        name: 'Jul',
        value: 750,
      },
      {
        name: 'Aug',
        value: 620,
      },
      {
        name: 'Sep',
        value: 680,
      },
      {
        name: 'Oct',
        value: 570,
      },
      {
        name: 'Nov',
        value: 590,
      },
      {
        name: 'Dec',
        value: 750,
      },
    ],
  },
];
