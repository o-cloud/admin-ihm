import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { UserService } from './services/user.service';
import { EnumToArrayPipe } from './pipes/enum-to-array.pipe';
import { StringEnumToArrayPipe } from './pipes/string-enum-to-array.pipe';

@NgModule({
  imports: [HttpClientModule],
  providers: [UserService, EnumToArrayPipe],
  declarations: [EnumToArrayPipe, StringEnumToArrayPipe],
  exports: [EnumToArrayPipe, StringEnumToArrayPipe],
})
export class CoreModule {}
