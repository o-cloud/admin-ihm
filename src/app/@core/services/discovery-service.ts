import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Identity } from '../models/identity.model';
import { Peer } from '../models/peer.model';
import { NetworkConnectionInfo, Network, NetworkStatus } from '../models/network.model';

export interface PeersResponse {
  peers: Peer[];
}

export interface IdentityResponse {
  identity: Identity;
}

export interface NetworkResponse {
  network: Network;
}

export interface NetworksResponse {
  networks: Network[];
}

export interface NetworkConnectionInfoResponse {
  connection_info: NetworkConnectionInfo;
}

export interface NetworkStatusResponse {
  status: NetworkStatus;
}

@Injectable({
  providedIn: 'root',
})
export class DiscoveryService {
  constructor(private http: HttpClient) {}

  private REST_API_SERVER = environment.urlDiscovery;

  public getPeers(): Observable<Peer[]> {
    return this.http
      .get<PeersResponse>(
        `${this.REST_API_SERVER}/peers`
      )
      .pipe(map((data) => data.peers));
  }

  public getIdentity(): Observable<Identity> {
    return this.http
      .get<IdentityResponse>(`${this.REST_API_SERVER}/identity`)
      .pipe(map((data) => data.identity));
  }

  public addIdentity(identity: Identity): Observable<Identity> {
    return this.http
      .post<IdentityResponse>(
        `${this.REST_API_SERVER}/identity`,
        { identity }
      )
      .pipe(map((data) => data.identity));
  }

  public updateIdentity(identity: Identity): Observable<Identity> {
    return this.http
      .put<IdentityResponse>(
        `${this.REST_API_SERVER}/identity`,
        { identity }
      )
      .pipe(map((data) => data.identity));
  }

  public deleteIdentity(): Observable<any> {
    return this.http.delete<any>(
      `${this.REST_API_SERVER}/identity`
    );
  }

  public getNetworks(): Observable<Network[]> {
    return this.http
      .get<NetworksResponse>(`${this.REST_API_SERVER}/networks`)
      .pipe(map((data) => data.networks));
  }

  public addNetwork(network: Network): Observable<Network> {
    return this.http
      .post<NetworkResponse>(
        `${this.REST_API_SERVER}/networks`,
        { network },
      )
      .pipe(map((data) => data.network));
  }

  public deleteNetwork(network: string): Observable<any> {
    return this.http.delete<any>(
      `${this.REST_API_SERVER}/networks/${network}`
    );
  }

  public getNetworksPeers(network: string): Observable<Peer[]> {
    return this.http
      .get<PeersResponse>(
        `${this.REST_API_SERVER}/networks/${network}/peers`
      )
      .pipe(map((data) => data.peers));
  }

  public getNetworksStatus(network: string): Observable<NetworkStatus> {
    return this.http
      .get<NetworkStatusResponse>(`${this.REST_API_SERVER}/networks/${network}/status`)
      .pipe(map((data) => data.status));
  }

  public getNetworksConnectionInfo(network: string): Observable<NetworkConnectionInfo> {
    return this.http
      .get<NetworkConnectionInfoResponse>(`${this.REST_API_SERVER}/networks/${network}/connection-info`)
      .pipe(map((data) => data.connection_info));
  }
}
