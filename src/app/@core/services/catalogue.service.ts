import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { environment } from '../../../environments/environment';
import { CatalogueResponse, ProviderType, CatalogueSearchResult } from '../models/catalog.models';

@Injectable({ providedIn: 'root' })
export class CatalogueService {
  socket: WebSocketSubject<any> | undefined;

  constructor(private http: HttpClient) {}

  getLocalProviders(type: ProviderType): Observable<CatalogueResponse[]> {
    return this.http.get<CatalogueResponse[]>(`${environment.urlCatalog}/${type}/search`);
  }

  search(terms: string, providerType: ProviderType): Observable<CatalogueSearchResult[]> {
    if (this.socket) {
      this.socket.complete();
    }

    this.socket = webSocket<any>({
      url: environment.webSocketSearchCatalog,
      openObserver: { next: () => console.log('open web socket') },
      closeObserver: { next: () => console.log('close web socket') },
    });

    this.socket.subscribe(
      (c) => console.log(c),
      (error) => console.log(error)
    );

    this.socket.next({
      terms,
      providerType,
    });

    return this.socket.pipe(
      map((res) => {
          return res.map((r: CatalogueResponse) => ({
            name: r.name,
            originClusterName: r.originClusterName,
            providerId: r.providerId,
            description: r.description,
            providerType: r.providerType,
            isFavorite: r.isFavorite,
            endpointLocation: r.endpointLocation,
            workflowStructure: r.workflowStructure,
            isLocal: r.isLocal,
            metadata: r.metadata
          }))
        }
      )
    );
  }
}
