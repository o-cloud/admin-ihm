import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import {Observable, of} from 'rxjs'
import { shareReplay } from 'rxjs/operators';

interface AuthConfiguration {
  enableKeycloakAuth: boolean,
  urlKeycloack: string,
  realmKeycloack: string,
  clientIdKeycloack: string,
}


@Injectable({
  providedIn: 'root'
})
export class DynamicConfigurationService {

  private configuration$!: Observable<AuthConfiguration>;

  public authConfiguration!: AuthConfiguration;

  constructor(private http: HttpClient, private keycloak: KeycloakService) {}

  public async load(): Promise<any>{
    console.log("loading config")
    if (!this.configuration$) {
      this.configuration$ = this.http
        .get<AuthConfiguration>(`/assets/config/auth/conf.json`)
        .pipe(shareReplay(1));
    }
    this.authConfiguration = await this.configuration$.toPromise()
    return this.initializeKeycloak(this.authConfiguration);
  }

  private async initializeKeycloak(conf : AuthConfiguration): Promise<boolean> {
    if (conf.enableKeycloakAuth) {
      return this.keycloak.init({
          config: {
            url: conf.urlKeycloack,
            realm: conf.realmKeycloack,
            clientId: conf.clientIdKeycloack,
          },
          initOptions: {
            onLoad: 'check-sso',
            silentCheckSsoRedirectUri: window.location.origin + '/assets/silent-check-sso.html',
          },
        }).then(r =>{;return r} );
    }
    return true;
  }
}
