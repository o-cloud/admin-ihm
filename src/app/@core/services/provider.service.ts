import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Permissions, Provider, Scope } from '../models/provider.model';

export interface ProvidersResponse {
  providers: Provider[];
}

export interface ProviderResponse {
  provider: Provider;
}

export interface ScopesResponse {
  scopes: {
    id: string;
    name: string;
    description: string;
    resources: { name: string }[];
    permissions: Permissions[];
    createdAt?: string;
    updatedAt?: string;
  }[];
}

export interface ResourcesResponse {
  resources: { name: string }[];
}

export interface ScopeResponse {
  scope: Scope;
}

export interface PermissionsResponse {
  permissions: Permissions;
}

@Injectable({
  providedIn: 'root',
})
export class ProviderService {
  constructor(private http: HttpClient) {}

  private REST_API_SERVER = environment.urlJsonServer;
  private REST_API_SERVER2 = environment.urlProviders;

  public getAllProviders(): Observable<Provider[]> {
    return this.http
      .get<ProvidersResponse>(`${this.REST_API_SERVER}/providers/`)
      .pipe(map((data) => data.providers));
  }

  public updateProvider(
    name: string,
    version: string,
    provider: Provider
  ): Observable<Provider> {
    return this.http
      .put<ProviderResponse>(
        `${this.REST_API_SERVER2}/${name}/${version}/provider`,
        { provider }
      )
      .pipe(map((data) => data.provider));
  }

  public addToCatalog(provider: Provider): Observable<Provider> {
    return this.http
      .post<ProviderResponse>(
        `${this.REST_API_SERVER2}/${provider.name}/${provider.version}/provider/register`,
        { provider }
      )
      .pipe(map((data) => data.provider));
  }

  public removeFromCatalog(provider: Provider): Observable<Provider> {
    return this.http
      .post<ProviderResponse>(
        `${this.REST_API_SERVER2}/${provider.name}/${provider.version}/provider/unregister`,
        { provider }
      )
      .pipe(
        map((data) => {
          return data.provider;
        })
      );
  }

  public getScopes(name: string, version: string): Observable<Scope[]> {
    return this.http
      .get<ScopesResponse>(
        `${this.REST_API_SERVER2}/${name}/${version}/provider/scopes`
      )
      .pipe(
        map((data) =>
          data.scopes.map((scope) => ({
            ...scope,
            resources: scope.resources.map((r) => r.name),
          }))
        )
      );
  }

  public getResources(name: string, version: string): Observable<string[]> {
    return this.http
      .get<ResourcesResponse>(
        `${this.REST_API_SERVER2}/${name}/${version}/resources`
      )
      .pipe(map((data) => data.resources.map((d) => d.name)));
  }

  public getPermissions(
    name: string,
    version: string
  ): Observable<Permissions> {
    return this.http
      .get<PermissionsResponse>(
        `${this.REST_API_SERVER2}/${name}/${version}/permissions`
      )
      .pipe(map((data) => data.permissions));
  }

  public addScope(provider: Provider, scope: Scope): Observable<Scope> {
    return this.http
      .post<ScopeResponse>(
        `${this.REST_API_SERVER2}/${provider.name}/${provider.version}/provider/scopes`,
        { scope }
      )
      .pipe(map((data) => data.scope));
  }

  public updateScope(provider: Provider, scope: Scope): Observable<Scope> {
    return this.http
      .put<ScopeResponse>(
        `${this.REST_API_SERVER2}/${provider.name}/${provider.version}/provider/scopes/${scope.id}`,
        { scope }
      )
      .pipe(map((data) => data.scope));
  }

  public deleteScope(provider: Provider, scope: Scope): Observable<any> {
    return this.http.delete<any>(
      `${this.REST_API_SERVER2}/${provider.name}/${provider.version}/provider/scopes/${scope.id}`
    );
  }
}
