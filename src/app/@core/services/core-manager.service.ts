import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Chart, ChartVersion, DeployChartParams, Metadata, Release, SimpleRelease } from '../models/cluster-config.models';

@Injectable({
  providedIn: 'root',
})
export class CoreManagerService {
  constructor(private http: HttpClient) {}

  private BASE_API_URL = `/api/core/manager`;

  public getDeployedModules(): Observable<SimpleRelease[]> {
    return this.http.get<SimpleRelease[]>(`${this.BASE_API_URL}/releases`);
  }

  public getReleaseDetail(name: string): Observable<Release> {
    return this.http.get<Release>(`${this.BASE_API_URL}/releases/${name}`);
  }

  public createRelease(params: DeployChartParams): Observable<string> {
    return this.http.post<string>(`${this.BASE_API_URL}/releases`, params)
  }

  public updateRelease(
    name: string,
    config: {[key:string]:any},
    version?: string
  ): Observable<any> {
    let params = {"version":version || ""}
    return this.http.patch<any>(`${this.BASE_API_URL}/releases/${name}`, config,{params});
  }

  public deleteRelease (name: string): Observable<any> {
    return this.http.delete<any>(`${this.BASE_API_URL}/releases/${name}`);
  }

  public getChartList(): Observable<Metadata[]> {
    return this.http.get<Metadata[]>(`${this.BASE_API_URL}/charts`);
  }

  public getChartDetail(name: string): Observable<Chart> {
    return this.http.get<Chart>(`${this.BASE_API_URL}/charts/${name}`);
  }

  public getChartVersions(name: string): Observable<ChartVersion[]> {
    return this.http.get<ChartVersion[]>(`${this.BASE_API_URL}/charts/${name}/versions`);
  }

  // Helper functions

  public isObject(item: any) {
    return item && typeof item === 'object' && !Array.isArray(item);
  }

  /**
   * Deep merge two objects.
   * @param target
   * @param ...sources
   */
  public mergeDeep(target: any, ...sources: any[]): any {
    if (!sources.length) return target;
    const source = sources.shift();

    if (this.isObject(target) && this.isObject(source)) {
      for (const key in source) {
        if (this.isObject(source[key])) {
          if (!target[key]) Object.assign(target, { [key]: {} });
          this.mergeDeep(target[key], source[key]);
        } else {
          Object.assign(target, { [key]: source[key] });
        }
      }
    }

    return this.mergeDeep(target, ...sources);
  }

  /**
   * Deep remove variables of two objects.
   * @param target to remove from
   * @param reference of what to remove
   */
  public removeDeep(target: any, reference: any) {
    for (const key in reference) {
      if (this.isObject(reference[key])) {
        if (target[key]) {
          target[key] = this.removeDeep(target[key], reference[key]);
          if (Object.keys(target[key]).length === 0) {
            delete target[key];
          }
        }
      } else if (target[key] == reference[key]) {
        delete target[key];
      }
    }

    return target;
  }
}
