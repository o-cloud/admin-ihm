import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { CostResource } from '../models/costresources.models';

@Injectable({
  providedIn: 'root',
})
export class KubecostService {
  private REST_API_SERVER = ''; // environment.urlJsonKubecost;
  constructor(private http: HttpClient) {}

  public getCostByCluster(windowPar: string): Observable<CostResource[]> {
    return this.http.get<CostResource[]>(
      this.REST_API_SERVER + '/model/aggregatedCostModel?window=' + windowPar
    );
  }
}
