import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { KeycloakAuthGuard, KeycloakService } from 'keycloak-angular';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard extends KeycloakAuthGuard {

  private requiredRoles = ["admin"]

  constructor(
    protected readonly router: Router,
    protected readonly keycloak: KeycloakService
  ) {
    super(router, keycloak);
  }

  public async isAccessAllowed(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    console.log("GUARDING")
    // Force the user to log in if currently unauthenticated.
    if (!this.authenticated) {
      await this.keycloak.login({
        redirectUri: window.location.origin + state.url,
      });
    }

    // Allow the user to proceed if all the required roles are present.
    console.log(this.roles);
    if (this.requiredRoles.every((role) => this.roles.includes(role))){
      return true;
    } else {
      this.router.navigateByUrl("/dashboard")
      return false;
    }
  }
}
