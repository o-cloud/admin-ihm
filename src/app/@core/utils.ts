function groupBy<T>(xs: T[], key: (o: T) => string): {[key: string]: T[]} {
  return xs.reduce((rv: {[key: string]: T[]}, x: T) => {
    (rv[key(x)] = rv[key(x)] || []).push(x);
    return rv;
  }, {});
}

export {
  groupBy
};
