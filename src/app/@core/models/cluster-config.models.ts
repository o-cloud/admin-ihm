export interface DeployChartParams {
  name: string;
  namespace: string;
  chartName: string;
  chartVersion: string;
  flags: {[key:string]: any};
  config: {[key:string]: any};
}

export interface SimpleRelease {
	name:          string
	namespace:     string
	version:       number
	metadata: Metadata
}


/** Metadata for a Chart file. This model maps the chart metadata from helm, but is not full */
export interface  Metadata  {
	/** The name of the chart. Required.*/
	name: string
	/** The URL to a relevant project page, git repo, or contact person*/
	home: string
	/** Source is the URL to the source code of this chart*/
	sources: string[]
	/** A SemVer 2 conformant version string of the chart. Required.*/
	version: string
	/** A one-sentence description of the chart*/
	description: string
	/** A list of string keywords*/
	keywords: string[]
	/** A list of name and URL/email address combinations for the maintainer(s)*/
	// //Non supported(yet) // Maintainers: []Maintainer
	/** The URL to an icon file.*/
	icon: string
	/** The API Version of this chart. Required.*/
	apiVersion: string
	/** The condition to check to enable chart*/
	condition: string
	/** The tags to check to enable chart*/
	tags: string
	/** The version of the application enclosed inside of this chart.*/
	appVersion: string
	/** Whether or not this chart is deprecated*/
	deprecated: boolean
	/** Annotations are additional mappings uninterpreted by Helm,
	made available for inspection by other applications. */
	annotations: {[key:string]: any}
  /**  KubeVersion is a SemVer constraint specifying the version of Kubernetes required.*/
	kubeVersion: string
	/**  Dependencies are a list of dependencies for a chart.*/
	// //Non supported(yet) //Dependencies: []*Dependency
	/**  Specifies the chart type: application or library */
	type: string
}



/** Chart model. This model maps the chart from helm, but is not full */
export interface Chart  {
	/** Metadata is the contents of the Chartfile. */
	metadata: Metadata
	/** Values are default config for this chart.*/
	values: {[key:string]: any}
	/** Schema is an optional JSON schema for imposing structure on Values */
	schema: string

	parent: Chart
	dependencies: Chart
}

/** ChartVersion represents a chart entry in the IndexFile */
export interface  ChartVersion extends Metadata {
	urls:    string
	created: Date
	removed: boolean
	digest: string

	/** ChecksumDeprecated is deprecated in Helm 3, and therefore ignored. Helm 3 replaced
	this with Digest. However, with a strict YAML parser enabled, a field must be
	present on the struct for backwards compatibility. */
	checksum: string

	/** EngineDeprecated is deprecated in Helm 3, and therefore ignored. However, with a strict
	YAML parser enabled, this field must be present. */
	engine: string

	/** TillerVersionDeprecated is deprecated in Helm 3, and therefore ignored. However, with a strict
	YAML parser enabled, this field must be present. */
	tillerVersion: string

	/** URLDeprecated is deprecated in Helm 3, superseded by URLs. It is ignored. However,
	with a strict YAML parser enabled, this must be present on the struct. */
	url: string
}



/** Release describes a deployment of a chart, together with the chart
and the variables used to deploy that chart.*/
export interface Release {
	/** Name is the name of the release*/
	name: string
	/** Info provides information about a release*/
	info: Info
	/** Chart is the chart that was released.*/
	chart: Chart
	/** Config is the set of extra Values added to the chart.
	These values override the default values inside of the chart.*/
	config: {[key:string]: any}
	/** Manifest is the string representation of the rendered template.*/
	manifest: string
	/** Version is an int which represents the revision of the release.*/
	version: number
	/** Namespace is the kubernetes namespace of the release.*/
	namespace: string
	/** Labels of the release.
	Disabled encoding into Json cause labels are stored in storage driver metadata field. */
	labels: {[key:string]: any}
}

/** Info describes release information. */
export interface Info {
	/** FirstDeployed is when the release was first deployed.*/
  first_deployed: Date
	/** LastDeployed is when the release was last deployed.*/
	last_deployed: Date
	/** Deleted tracks when this object was deleted.*/
	deleted: Date
	/** Description is human-friendly "log entry" about this release.*/
	description: string
	/** Status is the current state of the release*/
	status: string
	/** Contains the rendered templates/NOTES.txt if available */
	notes: string
}
