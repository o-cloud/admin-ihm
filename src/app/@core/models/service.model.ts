export interface Service {
    name: string;
    version: string;
    description: string;
    cwl: string;
    images: string[];
  }
