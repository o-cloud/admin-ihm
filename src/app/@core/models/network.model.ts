export interface Network {
  name: string;
  is_private: boolean;
  private_key: string | undefined;
  joining_nodes: string[] | undefined;
  entrypoint: string;
}

export interface NetworkConnectionInfo {
  network_name: string;
  private_key: string;
  peer_id: string[];
}

export interface NetworkStatus {
  state: NetworkState;
  is_identity_published: boolean;
}

export enum NetworkState {
  NotReady = "NotReady",
  Pending = "Pending",
  Ready = "Ready",
  Deleting = "Deleting",
}

export interface entrypoint {
  name: string;
  port: number;
}
