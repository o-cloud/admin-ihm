export interface ProviderFields {
  id: string;
  name: string;
  type: string;
}
