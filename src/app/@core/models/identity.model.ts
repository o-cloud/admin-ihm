export interface Identity {
  name: string;
  url: string;
  description: string;
  categories: string[];
  network: string;
  networks: string[];
}

export interface Swarmpool {
  network: string;
  name: string;
}
