export interface Provider {
  id: string;
  type: string;
  version: string;
  name: string;
  images: string[];
  description: string;
  metadata: string;
  createdAt?: string;
  updatedAt?: string;
  registeredAt?: string;
  unregisteredAt?: string;
  endpointLocation: string;
  workflowStructure: string;
  catalogSyncStatus: number;
  scopes: Scope[];
}

export interface Scope {
  id: string;
  name: string;
  description: string;
  resources: string[];
  permissions: Permissions[];
  createdAt?: string;
  updatedAt?: string;
}

export enum Permissions {
  ReadOnly = 'ReadOnly',
  WriteOnly = 'WriteOnly',
  ReadWrite = 'ReadWrite',
}

export enum ProviderType {
  Data,
  Service,
  Computing,
}

export interface ComputeProvider {
  id: string;
  version: string;
  name: string;
  description: string;
  statestiques: Usage[];
  cpuMax: string;
  ramMax: string;
  availableNodes: Node[];
  availableGPUs: GPU[];
}

export interface ServiceProvider {
  id: string;
  version: string;
  name: string;
  description: string;
  listService: SoftService[];
  statestiques: Usage[];
  reservations: Reservation[];
  cpuMax: string;
  ramMax: string;
}

export interface SoftService {
  id: number;
  name: string;
  version: string;
  date: Date;
  description: string;
  neededGPU: number;
  neededRAM: number;
}
export interface Usage {
  id: number;
  date: Date;
  description: string;
  gpu: number;
  cpu: number;
}

export interface Reservation {
  id: number;
  date: Date;
  description: string;
  gpu: number;
  cpu: number;
}

export interface Node {
  id: number;
  name: string;
  metadata: string;
  description: string;
}

export interface GPU {
  id: number;
  name: string;
  metadata: string;
  description: string;
}

export interface StorageProvider {
  id: string;
  providerId: string;
  providerType: string;
  version: string;
  name: string;
  description: string;
  metadata: string;
  endpointLocation: string;
  workflowStructure: string;
  diskSize: number; // in Mo
}
