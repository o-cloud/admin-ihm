export interface CostResource {
  aggregation: string;
  cpuAllocationAverage: string;
  cpuCost: string;
  cpuEfficiency: string;
  efficiency: string;
  environment: string;
  gpuAllocationAverage: string;
  gpuCost: string;
  networkCost: string;
  pvAllocationAverage: string;
  pvCost: string;
  ramAllocationAverage: string;
  ramCost: string;
  ramEfficienty: string;
  sharedCost: string;
  totalcost: string;
}
