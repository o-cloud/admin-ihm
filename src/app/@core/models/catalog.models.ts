export interface CatalogueResponse {
  originClusterName: string;
  description: string;
  endpointLocation: string;
  name: string;
  providerId: string;
  providerType: string;
  version: string;
  workflowStructure: string;
  isFavorite: boolean;
  isLocal: boolean;
  metadata: any;
}

export interface CatalogueSearchResult {
  name: string;
  originClusterName: string;
  providerId: string;
  description: string;
  providerType: string;
  isFavorite: boolean;
  endpointLocation: string;
  workflowStructure: string;
  isLocal: boolean;
  metadata: any;
}

export enum ProviderType {
  Data = 'data',
  Service = 'service',
  Computing = 'computing',
  Storage = 'storage',
}
