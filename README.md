# Admin IHM content

The project expose multiple page using an Angular SPA

## Dashboard

This page expose a condensed view of different informations the cluster:
- Networks info (identity, peers etc)
- Billing and data costs 
- Ressources

It should be specific by users

## Networks

This section contain two pages: Overview and Peers

Overview contain high level info about your current identity and the networks you've joined

Peers expose every peers that can be discovered accross all networks

## Local ressources

This section contain pages allow you to manage ressources provided by the current cluster, notably data, storage, services and compute

### Data

List all data sources provided by this cluster

### Storages

List all storage provided by this cluster

### Services

List all services provided by this cluster

### Compute

List all compute ressources provided by this cluster

## Cluster config

This sections help administrator manage version of microservices deployed on their cluster through graphical interface

## Resources Monitoring

This section aggregate all of the differente local ressource usage statistics



# Working on admin IHM

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.11.

## Development server

### Angular-cli

You can develop directly with angular cli and use live reload for fast iterations

Edit the file in the foolder `src/environments/environment.ts` to use local adresses that point toward the required microservices

Run `ng serve` directly for a dev server. Navigate to `http://localhost:4200/`.

### Skaffold

To test the applications directly integrated into a kubernetes cluster, run `skaffold run`

Make sure your environment file (`src/environments/environment.ts`) has the correct links.

Make sure you are point to the right kubernetes instance

## Build

The build is managed with docker

The push of image is automaticaly done through CI

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

